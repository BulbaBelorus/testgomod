package testgomod


import "fmt"
import "gitlab.com/BulbaBelorus/testgomod/somepack"


// PHello prints hello + name
func PHello(name string) {
	fmt.Println(somepack.Conc(name, ", hello!"))
}